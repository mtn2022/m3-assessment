import 'package:flutter/material.dart';
import 'profile.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                child: Text(
                  "Feature Screen 1",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return FeatureScreen1();
                  }));
                },
              ),
              FlatButton(
                child: Text(
                  "Feature Screen 2",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return FeatureScreen2();
                  }));
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text("Dashboard"),
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.person),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Profile();
            }));
          },
        ),
      ),
    );
  }
}

class FeatureScreen1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200,
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(16),
            color: Colors.red,
            child: Image.asset("assets/owl.jpg", fit: BoxFit.contain),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey[200],
            padding: EdgeInsets.only(top: 8, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
              width: double.infinity,
              child: Row(children: <Widget>[
                Container(
                  width: 150,
                  height: 100,
                  color: Colors.green,
                ),
                Expanded(
                  child: Container(
                    width: 150,
                    height: 100,
                    color: Colors.red,
                  ),
                  flex: 2,
                ),
                Container(
                  width: 150,
                  height: 100,
                  color: Colors.yellow,
                ),
              ]))
        ],
      ),
      appBar: AppBar(
        title: Text("Feature Screen 1"),
      ),
    );
  }
}

class FeatureScreen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200,
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(16),
            color: Colors.red,
            child: Image.asset("assets/owl.jpg", fit: BoxFit.contain),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey[200],
            padding: EdgeInsets.only(top: 8, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 35,
                    ),
                    Container(
                      height: 4,
                    ),
                    Text(
                      "CALL",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
              width: double.infinity,
              child: Row(children: <Widget>[
                Container(
                  width: 150,
                  height: 100,
                  color: Colors.green,
                ),
                Expanded(
                  child: Container(
                    width: 150,
                    height: 100,
                    color: Colors.red,
                  ),
                  flex: 2,
                ),
                Container(
                  width: 150,
                  height: 100,
                  color: Colors.yellow,
                ),
              ]))
        ],
      ),
      appBar: AppBar(
        title: Text("Feature Screen 2"),
      ),
    );
  }
}
